var emoticonsBaseUrl = "https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/";
var emoticons = {
  ";)": "wink.png",
  ":)": "smiley.png",
  "(y)": "like.png",
  ":*": "kiss.png",
  ":(": "sad.png"
};
var emoticonsRegex = /:\)|;\)|:\*|\(y\)|:\(/g;
var swearWordsRegex;
var swearWordsUrl = '/swearWords.txt';

function loadSwearWords() {
  $.ajax({
    type: 'GET',
    url: swearWordsUrl,
    async: false,
    success: function(words) {
       swearWordsRegex = new RegExp('\\b(' + words.split('\n').map(function(e){ return e.trim().replace(/\s/g, '\\s'); }).join('|') + ')\\b', 'g');
    }
  });
}

function blockSwearWords(msg) {
  if(swearWordsRegex)
    // regex uspesno nalozen
    msg = msg.replace(swearWordsRegex, function(match) { return match.replace(/./g, '*'); });

  return msg;
}

function spanTextElement(text) {
  return $('<span></span>').text(blockSwearWords(text));
}

function divEmoticons(msg) {
  if(typeof msg !== 'string' || msg.trim().length === 0)
    return msg;

  var msgWithEmoticon = $('<div style="font-weight: bold"></div>');
  var startIndex = 0;

  msg.replace(emoticonsRegex, function(match, index, str) {
    var text = str.substring(startIndex, index);
    if(text.length > 0)
      msgWithEmoticon.append(spanTextElement(text));
    if(emoticons[match])
      msgWithEmoticon.append('<img src="' + emoticonsBaseUrl + emoticons[match] + '" />');
    startIndex = index + match.length;
  });
  msgWithEmoticon.append(spanTextElement(msg.substring(startIndex, msg.length)));

  return msgWithEmoticon;
}

function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    var sporociloEl = divEmoticons(sporocilo);
    klepetApp.posljiSporocilo($('#ime-kanala').text(), sporociloEl.html());
    $('#sporocila').append(sporociloEl);
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  loadSwearWords();

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      $('#ime-uporabnika').text(rezultat.vzdevek);
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    $('#ime-kanala').text(rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    $('#sporocila').append($('<div style="font-weight: bold"></div>').html(sporocilo.besedilo));
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal !== '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  var seznamUporabnikov = $('#seznam-uporabnikov');

  socket.on('uporabniki', function(uporabniki) {
    seznamUporabnikov.empty();

    uporabniki.map(function(el) {
      if(el)
        seznamUporabnikov.append(divElementEnostavniTekst(el));
    });
  });

  socket.on('odstraniUporabnika', function(imeUporabnika) {
    seznamUporabnikov.find('div').each(function() {
      if($(this).text() === imeUporabnika)
        $(this).remove();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });

  window.onbeforeunload = function() {
    socket.emit('odjava');
    return null;
  };
});
