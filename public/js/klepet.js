var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(argumenti) {
  if(argumenti.length === 0)
    return;
  var data = {};
  if((argumenti.match(/"/g) || []).length === 4) {
    var trimArg = function(arg) {
      return arg.replace(/"/g, '').trim();
    };
    var argArray = argumenti.split('" "');
    data = {
      imeKanala: trimArg(argArray[0]),
      geslo: trimArg(argArray[1])
    };
    if(data.imeKanala.length === 0 || data.geslo.length === 0)
      return;
  }
  else
    data =  {
      imeKanala: argumenti,
      geslo: ''
    };

  this.socket.emit('pridruzitevZahteva', data);
};

Klepet.prototype.posljiZasebnoSporocilo = function(argumenti) {
  var argArray = argumenti.split('" "');
  // prekini posiljanje, ce ni 4 narekovajev za format: "<uporabnik>" "<sporocilo>"
  if((argumenti.match(/"/g) || []).length !== 4 || argArray.length !== 2)
    return 'Neznan ukaz.';

  var trimArg = function(arg) {
    return arg.replace(/"/g, '').trim();
  };

  var data = {
    ime: trimArg(argArray[0]),
    msg: divEmoticons(trimArg(argArray[1])).html()
  };
  this.socket.emit('zasebnoSporocilo', data);
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
   case 'zasebno':
      besede.shift();
      var argumenti = besede.join(' ');
      sporocilo = this.posljiZasebnoSporocilo(argumenti);
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  }

  return sporocilo;
};
