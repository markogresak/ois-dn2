var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var seznamKanalov = [];

function najdiKanal(ime) {
  for(var i = 0; i < seznamKanalov.length; i++)
    if(seznamKanalov[i].imeKanala === ime)
      return seznamKanalov[i];
}

exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj');
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    obdelajZasebnoSporocilo(socket);
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
};

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}

function pridruzitevKanalu(socket, kanal) {
  socket.join(kanal);
  trenutniKanal[socket.id] = kanal;
  socket.emit('pridruzitevOdgovor', {kanal: kanal});
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });

  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
  }

  prikazUporabnikov(socket, kanal);
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') === 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
        prikazUporabnikov(socket, trenutniKanal[socket.id]);
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo
    });
  });
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
    var ujemajocKanal = najdiKanal(kanal.imeKanala);

    if(ujemajocKanal && ujemajocKanal.geslo !== kanal.geslo) {
      // kanal ze obstaja, vendar se geslo ne ujema
      var odgovor = { besedilo: '' };

      if(ujemajocKanal.geslo === '')
        // kanal nima gesla
        odgovor.besedilo = 'Izbrani kanal ' + kanal.imeKanala + ' je prosto dostopen in ne zahteva prijave z geslom, zato se prijavite z uporabo /pridruzitev ' + kanal.imeKanala + ' ali zahtevajte kreiranje kanala z drugim imenom.';
      else
        // geslo je napacno
        odgovor.besedilo = 'Pridružitev v kanal ' + kanal.imeKanala + ' ni bilo uspešno, ker je geslo napačno!';

      return socket.emit('sporocilo', odgovor);
    }

    // ce pridemo do tukaj je geslo pravilno, ali pa kanal se ne obstja
    if(!ujemajocKanal)
      // ce kanal se ne obstaja, ga dodamo na seznam
      seznamKanalov.push(kanal);
    odstraniUporabnika(socket, vzdevkiGledeNaSocket[socket.id]);
    socket.leave(trenutniKanal[socket.id].imeKanala);
    pridruzitevKanalu(socket, kanal.imeKanala);
  });
}

function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    odstraniUporabnika(socket, uporabljeniVzdevki[vzdevekIndeks]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
}

function prikazUporabnikov(socket, kanal) {
  var data = io.sockets.clients(kanal).map(function(u) {
    return vzdevkiGledeNaSocket[u.id];
  });

  io.sockets.in(kanal).emit('uporabniki', data);
}

function odstraniUporabnika(socket, ime) {
  socket.broadcast.emit('odstraniUporabnika', ime);
}

function obdelajZasebnoSporocilo(socket) {
  socket.on('zasebnoSporocilo', function(data) {
    var odgovor = { besedilo: 'Sporočilo "' + data.msg + '" uporabniku z vzdevkom ' + data.ime + ' ni bilo mogoče posredovati.' };
    if(data.ime !== vzdevkiGledeNaSocket[socket.id])
      for(var key in vzdevkiGledeNaSocket)
        if(vzdevkiGledeNaSocket.hasOwnProperty(key))
          if(data.ime === vzdevkiGledeNaSocket[key]) {
            io.sockets.socket(key).emit('sporocilo', {
              besedilo: vzdevkiGledeNaSocket[socket.id] + ' (zasebno): ' + data.msg
            });
            odgovor.besedilo = '(zasebno za ' + data.ime + '): ' + data.msg;
            break;
          }

    socket.emit('sporocilo', odgovor);
  });
}
